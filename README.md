# Ecosystem Governance Compass v2.0 JYU

The Ecosystem Governance Compass is a domain-specific modeling language for visualization, discussion, brainstorming, designing collaborative ecosystems and their governance. The compass tool is based on domain-specific modeling, and it is implemented in MetaEdit+.

The compass is developed in StartupLab JYU by a researcher team in University of Jyv�skyl�, Finland. The tool is based on theory and is developed by using design science research methodology. Currently the Ecosystem Governance Compass is in its second developmental phase.

Contact\
_Gabriella Laatikainen, postdoctoral researcher_\
gabriella.g.laatikainen@jyu.fi\
*[Blockchain and Digital Identity research, StartupLab JYU](http://www.startuplab.fi/research/blockchain-and-digital-identity-lab/)*

## 1 Using the Ecosystem Governance Compass � Modeling with MetaEdit+

### Getting started with MetaEdit+ domain-specific modeling tool

The Ecosystem Governance Compass is currently available only with a commercial domain-specific modeling tool called MetaEdit+ by MetaCase. The tool is available as a free 31-day evaluation version that is fully functional. MetaEdit+ is available for Windows and Linux.

Detailed instructions for using the tool in *[MetaEdit+ User�s Guide](https://www.metacase.com/support/45/manuals/meplus/Mp.html)*

#### Downloading MetaEdit+ for Windows and Linux users

1. Download the MetaEdit+ tool from *[metacase.com/download]( https://www.metacase.com/download/metaedit/)*.
2. Select a MetaEdit+ Workbench platform for Windows or Linux and fill the form for receiving instructions and passwords for downloading programs.
3. Follow the instructions sent by email.
4. When uninstalling the program, please note that your repository and generated files are not deleted by uninstallation. If you want to remove them, delete your Documents\MetaEdit+ 5.5 folder.

#### Importing the language and existing models

1. Login to **"demo"** repository (no need to open any project from the list on the right-side column).
2. Create a new project: **Repository** | **New Project ...** and enter a name for your project (e.g., Verifiable Organizations Network).
3. Choose it as default project: pulldown list, bottom left.
4. Import the .met file (language) and the .mxs file (symbols) by repeating: **Repository** | **Import** and select the files provided in Ecosystem Governance Compass GitLab.
   4.1. If you want to import existing models, import the .mxm files as well repeating **Repository** | **Import** and select the files provided in Ecosystem Governance Compass GitLab.
5. Importing is finished when the project is highlighted in the **"Projects" list**, and you can see all the imported models in the Graph **Browser** | **Graphs window**.
6. Remember to commit Repository | Commit.

### Generating documentation from the models

MetaEdit+ has multiple generators that access information in the repository and transforms it into text-based outputs such as code, documentation and checking reports.

Generating documentation from a single model (graph)
1. Open the graph from which you want to generate from.
2. Choose **Graph** | **Generate�**.
3. Select the generators that you want to run and execute the generator.
5. You can view the generated output on the **Generator Output** tool.

Word documents include the diagrams and hyperlinks between design elements as well as all object descriptions. HTML files for web browsers include the diagrams as PNG pictures, documentation and properties of design elements, and hyperlinks between them.

MetaEdit+ also allows you to print system designs through the Graph | Print� command, and **File** | **Print�** in the Generator Output tool.

Read more at MetaEdit+ User's Guide: *[5.1 Design data output and exchange tools: Generating](https://www.metacase.com/support/45/manuals/meplus/Mp.html)*

### How to make sure you won't make accidental changes to the language

Administrators can specify which users are allowed to change language definitions. We recommend you restrict your metamodeling rights to make sure that you don't make any changes to the Blockchain Governance Compass by an accident.

1. Login to "demo" repository as a System Administrator Repository User: sysadmin, radio button list, bottom left.
2. Go to repository settings: **Repository** | **Options** | **Repository**.
3. Press "Metamodelers" and make sure that only "sysadmin" is chosen (highlighted in blue) from the list.
4. Commit the changes **Repository** | **Commit** and exit.
5. From now on make sure that you login to **"demo"** repository as a User Repository User: user. You have access to language definition tools, but you can't save any changes.

## 2 Collaborative modeling with MetaEdit+

The free evaluation version of MetaEdit+ does not support multiple users sharing and editing the same models at the same time. However, all the versions have a modeling history, and changes are automatically recorded and traceable.

### Collaborative modeling: the basics

With multiple developers interacting on a large project, you must cope with integrating the work of several simultaneous developers on a single module.

The best approach is to divide the workload and give each user their own graph to be modeled. The first user edits a graph, exports the graph, and others have to either ...

1. wait until the previous modeler is finished, then import the updated graph and continue editing one at a time, or
2. merge their already done changes with the imported version. Merging happens by copy-pasting the changes into the graph they want to update and then deleting the outdated version.

Be mindful if you choose to **merge** instead of **taking turns** in editing. Try to avoid merging as much as possible as it is error-prone and tedious. And finally, name your exports explicitly (date, editor, graph type,...), use comments for managing the work, communicate with your team etc.

Read more at MetaEdit+ User's Guide: *[3.4. Changes & Versions Tool and 3.5. Version control integration](https://www.metacase.com/support/55/manuals/meplus/Mp.html)*

|![MetaEdit+, collaborative modeling](images/merging-small.png)|
:--:
|Figure: Kelly, S. (2017). Collaborative modeling with version control. In Federation of International Conferences on Software Technologies: Applications and Foundations (pp. 20-29). Springer, Cham.|

### How to export a single graph to XML

1. Commit your changes to the repository
2. Go to the **Graph Manager** (**Browser** | **Graph Manager**)
3. Select the graph to be exported from the left-hand list by clicking it with the left mouse button.
4. Press the arrow button between the lists. This updates the selected items to be included in the export file.
5. Press **�Export to XML�** button (results a .mxm file).
6. Give an explicit name for the export file.

Read more at MetaEdit+ User's Guide: *[5. Design data output and exchange tools](https://www.metacase.com/support/45/manuals/meplus/Mp.html)*

### How to import a single graph

1. If you want to keep the previous graph for e.g., copy�pasting elements, rename the previous graph before importing the updated graph.
2. Choose **Repository** | **Import**.
3. Choose the XML file to be imported.
4. When you do not need the previous graph, delete it by right�clicking on the previous file and choose Delete graph.
5. Remember to commit the changes.

Read more at MetaEdit+ User's Guide: *[5. Design data output and exchange tools](https://www.metacase.com/support/45/manuals/meplus/Mp.html)*

----

**_Ecosystem Governance Compass v2_**

In case of problems or feedback, contact the development team\
_Gabriella Laatikainen, postdoctoral researcher_\
gabriella.g.laatikainen@jyu.fi\
*[Blockchain and Digital Identity research, StartupLab JYU](http://www.startuplab.fi/research/blockchain-and-digital-identity-lab/)*
