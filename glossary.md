# Ecosystem Governance Compass v2 Glossary

The Ecosystem Governance Compass is a domain-specific modeling language for visualization, discussion, brainstorming, designing collaborative ecosystems and their governance.

## General elements in light grey

**Main Container**\
The grouping element aggregates or composes concepts that belong together based on some common characteristic. One for each 4 main labels: governance, business, technology, legal & regulatory context.

**Sub-Container**\
The grouping element aggregates or composes concepts that belong together based on some common characteristic.\
Governance container: actors and roles, rights, rules and responsibilities, incentives.\
Business container: business activities, financial aspects.\
Technology container: data, architecture, services, and their components.\
Legal and regulatory container: laws, acts and regulations, standards."

**Comment**\
General comment.

**Joker**\
Joker allows you to express data that you did not find possible with current language. Give also more detailed explanation about the use of Joker.

## Governance elements in Purple

**Actor**\
An actor is an entity who is capable of performing behaviors or activities in the ecosystem. E.g. individual, business, organization, community or authority.

**Role**\
A characteristic set of behaviours or activities undertaken by ecosystem actors. E.g. verifier, issuer, holder, developer, etc.

**Responsibility**\
A behaviour or action that actors or roles can be held accountable for. E.g., maintaining a node, hosting digital wallets.
Right
A right or privilege to perform a certain behaviour or activity. E.g., access, development, voting, decision, ownership, control rights, etc.

**Rule**\
A regulation or principle that governs conduct in the ecosystem. E.g., Development or governance rules, etc.

**Incentive**\
Motivational factors of the actors or roles to take actions. E.g., Non-monetary could be e.g., reputation, visibility, shared norms, gaining experience, networking, collaborating, etc.

**Risk**\
E.g., Lack of information, credential issued without sufficient basis, inappropriate actors allowed to participate, etc.

## Business elements in Yellow

**Business Activity**\
A collection of business behaviour that an actor or role can perform to create and capture value. E.g., Marketing, node hosting, providing specific services, development, sales, production/manufacturing, accounting, logistics, etc.

**Cost**\
A financial representation of a cost incurred due to an asset, resource, activity, or service necessary for value creation or capture. E.g., Hardware costs, cloud costs, coordination cost, migration cost, training costs, sales costs, search costs, negotiation costs, etc.

**Revenue Model**\
Incoming money stream and the related pricing elements through which an actor or a role captures value. E.g., Usage fee, subscription, licensing, advertising, micropayments, etc.

## Technology elements in Green

**Platform**\
In this context, a platform refers to technological architecture (tools, libraries, and reusable components) that facilitate value creation. E.g., Corda, Ethereum Enterprise, Hyperledger Indy, Sovrin MainNet, etc.

**Framework**\
Software libraries (components, interfaces and tools) that enable the development of a technological solution. E.g., Exonum, Hyperledger Fabric, Openchain, etc.

**Middleware**\
Software that facilitates the integration of different components within a unified interface. E.g., Chainlink, Aries Cloud Controller, etc.

**Application**\
A technological component that is built to encapsulate and perform a functionality. E.g., developed applications such as phone apps; software element, highest layer for the technology stack.

**Service**\
An explicitly defined exposed technology behaviour. E.g., Different network services, e.g., agency services, value tokenization, etc.

**Smart contract**\
A piece of code that represents a self-executing digital contract. They are automatic rules that are embedded into technology. E.g., Rule validation, identity implementation, credential revocation, credential issuance, etc.

**Consensus mechanism**\
Name*, Properties, Description
A fault-tolerant method of authenticating and validating a value or transaction on a distributed ledger. E.g., Proof-of-stake, proof-of-work, decided between nodes, etc.

**Data Object**\
A data object represents a piece of information structured for automated processing. E.g., A verifiable credential (VC), public DIDs, e-receipts, etc.

**Data Storage**\
The collection and retention of digital information. E.g., Off-ledger (business) databases, public databases, wallet, etc.

## Legal and regulatory elements in Orange

**Agreement**\
Legally enforceable contract among ecosystem actors. E.g., Trade agreements, business contracts among the actors, foundation bylaws, etc.

**Regulation**\
A set of rules created and enforced through social or governmental institutions to regulate certain aspects or behaviour in the ecosystem. E.g., Payment institution act, national laws, GDPR, etc.

**Standard**\
A set of specification, established norm, guideline or requirement that serves to ensure compatibility, safety and quality of certain aspects or components of the ecosystem. E.g., Technical standards, industry standards, etc.

**Association**\
An association between two elements, currently in use: is, has, uses, provides, applies to, complies with, is associated with). For example, an Actor has a Role, a Role performs a Business Activity, a Platform provides a Service.
